-- MySQL dump 10.14  Distrib 5.5.68-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: snap
-- ------------------------------------------------------
-- Server version	5.5.68-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ast`
--

DROP TABLE IF EXISTS `ast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ast` (
  `ID` smallint(5) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `full_name` varchar(200) NOT NULL DEFAULT '',
  `device` varchar(150) NOT NULL DEFAULT '',
  `domain` varchar(35) NOT NULL DEFAULT '',
  `family` varchar(35) NOT NULL DEFAULT '',
  `member` varchar(35) NOT NULL DEFAULT '',
  `att_name` varchar(50) NOT NULL DEFAULT '',
  `data_type` tinyint(1) NOT NULL DEFAULT '0',
  `data_format` tinyint(1) NOT NULL DEFAULT '0',
  `writable` tinyint(1) NOT NULL DEFAULT '0',
  `max_dim_x` smallint(6) unsigned NOT NULL DEFAULT '0',
  `max_dim_y` smallint(6) unsigned NOT NULL DEFAULT '0',
  `levelg` tinyint(1) NOT NULL DEFAULT '0',
  `facility` varchar(45) NOT NULL DEFAULT '',
  `archivable` tinyint(1) NOT NULL DEFAULT '0',
  `substitute` smallint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`),
  KEY `ID_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Attribute Snap Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ast`
--

LOCK TABLES `ast` WRITE;
/*!40000 ALTER TABLE `ast` DISABLE KEYS */;
INSERT INTO `ast` VALUES (1,'2020-12-30 00:00:00','sys/tg_test/1/ampli','sys/tg_test/1','sys','tg_test','1','ampli',5,0,2,1,0,0,'HOST:port',0,0),(2,'2020-12-30 00:00:00','sys/tg_test/1/double_scalar','sys/tg_test/1','sys','tg_test','1','double_scalar',5,0,3,1,0,0,'HOST:port',0,0);
/*!40000 ALTER TABLE `ast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `context`
--

DROP TABLE IF EXISTS `context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `context` (
  `id_context` smallint(6) NOT NULL AUTO_INCREMENT,
  `time` date NOT NULL DEFAULT '0000-00-00',
  `name` varchar(128) NOT NULL DEFAULT '',
  `author` varchar(64) NOT NULL DEFAULT '',
  `reason` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id_context`),
  UNIQUE KEY `id_context_2` (`id_context`),
  KEY `id_context` (`id_context`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `context`
--

LOCK TABLES `context` WRITE;
/*!40000 ALTER TABLE `context` DISABLE KEYS */;
INSERT INTO `context` VALUES (1,'2020-12-30','TEST_CONTEXT','Piotr Goryl','Testing SNAP','Test'),(2,'2020-12-30','TEST_ALARM_FAST','AlarmAPP','ALARM','sys/tg_test/1/ampli > 4.0'),(3,'2020-12-30','TEST_ALARM','AlarmAPP','ALARM','sys/tg_test/1/ampli > 4.0');
/*!40000 ALTER TABLE `context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list`
--

DROP TABLE IF EXISTS `list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list` (
  `id_context` smallint(6) NOT NULL DEFAULT '0',
  `id_att` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list`
--

LOCK TABLES `list` WRITE;
/*!40000 ALTER TABLE `list` DISABLE KEYS */;
INSERT INTO `list` VALUES (1,1),(1,2),(2,1),(3,1);
/*!40000 ALTER TABLE `list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `snapshot`
--

DROP TABLE IF EXISTS `snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `snapshot` (
  `id_snap` smallint(6) NOT NULL AUTO_INCREMENT,
  `id_context` mediumint(9) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT '2005-06-30 22:00:00',
  `snap_comment` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_snap`),
  KEY `id_snap` (`id_snap`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `snapshot`
--

LOCK TABLES `snapshot` WRITE;
/*!40000 ALTER TABLE `snapshot` DISABLE KEYS */;
INSERT INTO `snapshot` VALUES (1,1,'2020-12-30 13:28:12',''),(2,2,'2020-12-30 14:44:23','ALARM: Simple alarm on a fast device'),(3,3,'2020-12-30 14:45:04','ALARM: The simplest test alarm'),(4,2,'2020-12-30 14:45:17','ACKNOWLEDGED: ACKNOWLEDGED'),(5,3,'2020-12-30 14:45:32','ACKNOWLEDGED: ACKNOWLEDGED'),(6,2,'2020-12-30 14:45:48','ACKNOWLEDGED: RESET'),(7,3,'2020-12-30 14:45:55','ACKNOWLEDGED: RESET'),(8,2,'2021-01-07 18:10:29','ALARM: Simple alarm on a fast device'),(9,2,'2021-01-07 18:46:59','ALARM: Simple alarm on a fast device'),(10,2,'2021-01-07 19:01:02','ACKNOWLEDGED: ACKNOWLEDGED'),(11,2,'2021-01-07 20:25:11','ALARM: Simple alarm on a fast device'),(12,2,'2021-01-07 20:39:30','ALARM: Simple alarm on a fast device'),(13,2,'2021-01-07 20:41:20','ALARM: Simple alarm on a fast device'),(14,2,'2021-01-08 07:24:59','ALARM: Simple alarm on a fast device'),(15,2,'2021-01-08 09:07:17','ALARM: Simple alarm on a fast device'),(16,3,'2021-01-08 09:07:20','ALARM: The simplest test alarm'),(17,2,'2021-01-08 11:49:28','ALARM: Simple alarm on a fast device'),(18,3,'2021-01-08 11:49:30','ALARM: The simplest test alarm'),(19,2,'2021-01-08 12:13:35','ALARM: Simple alarm on a fast device'),(20,3,'2021-01-08 12:15:35','ACKNOWLEDGED: RESET'),(21,2,'2021-01-08 12:16:15','ACKNOWLEDGED: RESET'),(22,2,'2021-01-08 12:17:17',''),(23,3,'2021-01-08 12:17:18','ALARM: The simplest test alarm'),(24,3,'2021-01-08 12:19:16','ACKNOWLEDGED: ACKNOWLEDGED'),(25,2,'2021-01-08 12:21:29','ACKNOWLEDGED: ACKNOWLEDGED'),(26,2,'2021-01-08 14:36:12','ALARM: Simple alarm on a fast device'),(27,3,'2021-01-08 14:36:30','ALARM: The simplest test alarm'),(28,2,'2021-01-08 15:54:26','ALARM: Simple alarm on a fast device'),(29,3,'2021-01-08 15:54:38','ALARM: The simplest test alarm'),(30,2,'2021-01-08 19:31:29','ALARM: Simple alarm on a fast device'),(31,3,'2021-01-08 19:31:41','ALARM: The simplest test alarm'),(32,2,'2021-01-08 19:59:01','ACKNOWLEDGED: RESET'),(33,3,'2021-01-08 19:59:11','ACKNOWLEDGED: RESET'),(34,2,'2021-01-08 20:00:55','ALARM: Simple alarm on a fast device'),(35,3,'2021-01-08 20:01:07','ALARM: The simplest test alarm');
/*!40000 ALTER TABLE `snapshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_im_1val`
--

DROP TABLE IF EXISTS `t_im_1val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_im_1val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `dim_x` smallint(6) NOT NULL,
  `dim_y` smallint(6) NOT NULL,
  `value` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for Image, Read only attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_im_1val`
--

LOCK TABLES `t_im_1val` WRITE;
/*!40000 ALTER TABLE `t_im_1val` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_im_1val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_im_2val`
--

DROP TABLE IF EXISTS `t_im_2val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_im_2val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `dim_x` smallint(6) NOT NULL,
  `dim_y` smallint(6) NOT NULL,
  `read_value` longblob,
  `write_value` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for Image, Write only attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_im_2val`
--

LOCK TABLES `t_im_2val` WRITE;
/*!40000 ALTER TABLE `t_im_2val` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_im_2val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sc_num_1val`
--

DROP TABLE IF EXISTS `t_sc_num_1val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sc_num_1val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `value` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for Scalar, Read only attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sc_num_1val`
--

LOCK TABLES `t_sc_num_1val` WRITE;
/*!40000 ALTER TABLE `t_sc_num_1val` DISABLE KEYS */;
INSERT INTO `t_sc_num_1val` VALUES (1,1,0),(2,1,12),(3,1,12),(4,1,12),(5,1,12),(6,1,2),(7,1,2),(8,1,5),(9,1,10),(10,1,10),(11,1,10),(12,1,10),(13,1,10),(14,1,10),(15,1,11),(16,1,11),(17,1,11),(18,1,11),(19,1,11),(20,1,1),(21,1,1),(22,1,11),(23,1,11),(24,1,11),(25,1,11),(26,1,5),(27,1,5),(28,1,10),(29,1,10),(30,1,10),(31,1,10),(32,1,0),(33,1,0),(34,1,10),(35,1,10);
/*!40000 ALTER TABLE `t_sc_num_1val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sc_num_2val`
--

DROP TABLE IF EXISTS `t_sc_num_2val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sc_num_2val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `read_value` double NOT NULL DEFAULT '0',
  `write_value` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for Scalar, Read/Write + Read with Write attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sc_num_2val`
--

LOCK TABLES `t_sc_num_2val` WRITE;
/*!40000 ALTER TABLE `t_sc_num_2val` DISABLE KEYS */;
INSERT INTO `t_sc_num_2val` VALUES (1,2,-66.2569646932381,0);
/*!40000 ALTER TABLE `t_sc_num_2val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sc_str_1val`
--

DROP TABLE IF EXISTS `t_sc_str_1val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sc_str_1val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for String, Read only attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sc_str_1val`
--

LOCK TABLES `t_sc_str_1val` WRITE;
/*!40000 ALTER TABLE `t_sc_str_1val` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sc_str_1val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sc_str_2val`
--

DROP TABLE IF EXISTS `t_sc_str_2val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sc_str_2val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `read_value` varchar(255) DEFAULT NULL,
  `write_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for String, Read/Write + Read with Write attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sc_str_2val`
--

LOCK TABLES `t_sc_str_2val` WRITE;
/*!40000 ALTER TABLE `t_sc_str_2val` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sc_str_2val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sp_1val`
--

DROP TABLE IF EXISTS `t_sp_1val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sp_1val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `dim_x` smallint(6) NOT NULL,
  `value` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for Spectrum, Read only attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sp_1val`
--

LOCK TABLES `t_sp_1val` WRITE;
/*!40000 ALTER TABLE `t_sp_1val` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sp_1val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sp_2val`
--

DROP TABLE IF EXISTS `t_sp_2val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sp_2val` (
  `id_snap` int(11) NOT NULL DEFAULT '0',
  `id_att` int(11) NOT NULL DEFAULT '0',
  `dim_x` smallint(6) NOT NULL,
  `read_value` blob,
  `write_value` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for Spectrum, Read/Write attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sp_2val`
--

LOCK TABLES `t_sp_2val` WRITE;
/*!40000 ALTER TABLE `t_sp_2val` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sp_2val` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-08 21:40:27
