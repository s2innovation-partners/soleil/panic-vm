import panic
alarms = panic.api()

tag = 'Tango_Test_Ampli_ALARM'
device = 'panic/examples/pyalarm_standard'
alarm_formula='sys/tg_test/1/ampli > 2'
alarm_description='Ampli_to_high'
alarm_receivers='operators@synchrotron-soleil.fr'

alarms.add(tag, device, formula = alarm_formula, description = alarm_description, receivers = alarm_receivers)

for alarm in alarms.get(tag='', device='', attribute='', receiver=''):
    print(alarm)
	
