import panic
alarms = panic.api()

tag = 'Tango_Test_Ampli_ALARM'
updated_formula = 'sys/tg_test/1/ampli > 5'

alarms[tag].formula = updated_formula
alarms[tag].write()
