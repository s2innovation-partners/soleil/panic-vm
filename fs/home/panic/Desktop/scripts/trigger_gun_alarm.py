#!/usr/bin/python
from tango import DeviceProxy

beam = DeviceProxy("elin/beam/run")
beam.on()

gun_ps = DeviceProxy("elin/gun/hv")
gun_ps.off()