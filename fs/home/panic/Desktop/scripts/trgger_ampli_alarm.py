from tango import DeviceProxy

tango_test = DeviceProxy("sys/tg_test/1")
update_value = 5

scalar = tango_test.ampli

print("Current ampli value = {0}".format(scalar))
print("Update ampli value to {0}".format(update_value))

tango_test.ampli = update_value
print(tango_test.status())

