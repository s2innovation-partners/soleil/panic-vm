# Content of the VM for PANIC training

**Licnese:** [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

**Author:** [Piotr Goryl](mailto:piotr.goryl@s2innovation.com), [S2Innovation Sp. z o. o.](www.s2innovation.com)

## The OS

The VM is CentOS 7 based and uses MAX-IV repositories for Tango Controls related packages.

## Tango

The Tango Controls is installed from rpm packages according to [the documentation](https://tango-controls.readthedocs.io/en/latest/installation/tango-on-linux.html?highlight=MAX-IV#centos).

- /etc/systemd/system/tango-db.service file has been craetd with the following content:
  
  ```conf
  .include /usr/lib/systemd/system/tango-db.service

  [Service]
  ExecStart=
  ExecStart=/usr/bin/DataBaseds 2 -ORBendPoint giop:tcp::10000
  ```

- in /usr/local/share a symbolic lik to /usr/share/pogo is created to streamline
  compilation of a linac simulation:

  ```bash
  cd /usr/local/share
  ln -s /usr/share/pogo
  ```

- `/opt/dservers` folder has been created for device servers binaries

## Python packages

- PyTango is installed from the MAX-IV rpm (9.3.1),
- PyQt4, GuiQwt, MySQL-python, as dependencies for Taurus and PANIC GUI are installed from the official CentOS repository:
  
  ```bash
  sudo yum install python-pyqt4
  sudo yum install python-guiqwt
  sudo yum install MySQL-python
  ```

- PANIC 7.5.1 and its dependencies are installed from PyPI as the `sudo` user (in `/usr` prefix). The dependencies include:

  - Taurus - 4.7
  - PyTangoArchiving - 8.12.2
  - fandango - 14.12.0
  
  Preparation to install (*pip* and *setuptools* update):

  ```bash
  sudo pip install --upgrade pip
  sudo pip install --upgrade setuptools
  ```

  , packages installation:

  ```bash
  sudo pip install panic
  sudo pip install pytangoarchiving
  ```

## SNAPshot database for alarms' history

The SNAP is installed from [ArchivingRoot-16.2.4](https://sourceforge.net/projects/tango-cs/files/tools/ArchivingRoot-16.2.4.zip) in `/opt/archiving`.

- The `ARCHIVING_ROOT` is set in `/etc/tangorc` to `/opt/archiving`,

- Device servers staring scripts from `/opt/archiving/devices/bin/linux` are set executables (`chmod +x *`) and copied to `/opt/dservers`,

- A Beniskin launcher from `/opt/archiving/bin` is set to executable and copied to `~/.local/bin`,

- A database has been created with a delivered script in `../archiving/db` : `mysql -u root -p < create-SNAPDB-InnoDB.sql`,

- Tango devices has been configured following *tangobox* configuration (see the SQL backup).

## Simulation environment

The ESRF Linac simulation is provided as an example. It is based on sources available in `tangobox`.

The sources has been extracted from the *tangobox* repository to the `~/work/linac` folder.

### Device Servers

To compile and install of the simulation, the following has been done:

- two files: `bin/make.all` and `bin/linac.copy` has been modified with proper paths,
- also `LinacMediumLevel` Makefile has been adjusted with paths to other device servers,
- the package `flex-devel` has been installed for *LinacSequencer* to compile:
  
  ```bash
  sudo yum install flex-devel
  ```
  
- then:

  ```bash
  cd ~/work/linac/bin
  ./make.all
  sudo ./linac.copy
  ```

- then, files from `config_file_jive_fo_simu/` have been loaded into Jive,

### GUI

The GUI is available in `~/work/linac/JLInac`.  

- `*.jar` files have been copied to `~/.local/share/java`,
- the `jlinac` file has been adjusted with proper paths and places under `~/.local/bin/`
  
## Databases

Databases' backup is stored in the `/db` folder of this repository.

## Desktop launchers

For convinience, desktop lunchers has been created for:

- Jive
- Astor
- JLinac
- PANIC GUI
- Bensikin
- itango
  
See, `*.desktop` files in `/home/panic/Desktop`. Icons are places in `~/Pictures/icons`.

## Elettra Alarm Handelr

The Elettra Alarm Handler device server is installed from [sources](https://github.com/ELETTRA-SincrotroneTrieste/alarm-handler):

```bash
cd ~/work/elettra
git clone https://github.com/ELETTRA-SincrotroneTrieste/alarm-handler.git
git clone https://github.com/ELETTRA-SincrotroneTrieste/makefiles.git
```

- `makefiles/Make-9.3.3.in` has been edited in the lines 6 to 9, with the following content:

  ```Makefile
  TANGO_DIR := /usr
  OMNIORB_DIR := /usr
  ZMQ_DIR := /usr
  RUNTIME_DIR := /runtime
  ```

- then, the device server has been compiled and the result has been copied to `/opt/dservers`:
  
  ```bash
  cd alarm-handler
  make
  sudo cp bin/* /opt/dservers
  ```

A server `alarm-handler-srv/elettra` has been added to Tango, with *Astor*. Device `panic/demo/elettra` is added as well.

There are also old Elettra `alarm-srv` and `alarmtest-srv` compiled with a related GUI. See documentation in `~/work/elettra`.

## MAX-IV ELK based application

The VM is fitted with a demo of MAX-IV PyAlarm device server integrated with the ELK stack.

MAX-IV uses its own version of PyAalrm extended with a facility to send events to the logstash/elasticsearch. 
The DS is based on an old PyAlarm version (4.22.13). To avoid conflict with the mainstream *PyAlarm* (installed in `/usr`), 
the MAX-IV one is installed in a virtual environment `~/demo/maxiv/env`. It is not configured in the `Starter`. 
So, it has to be started manually to be used. For this purpose a script is provided: `demo/maxiv/start.sh`.

The script starts ELK related services (*elasticsearch*, *logstash*, *kibana*), too. The services are disabled for autostart to avoid resources consumption (these are java based and need at least 256m heap size).

### ELK installation

The ELK is installed, following this [instruction](https://computingforgeeks.com/how-to-install-elk-stack-on-centos-fedora/), skipping a java installation step as it has been installed with tango packages.

The memory assignment has been reduced to:

```txt
-Xms256m
-Xmx512m
```

, in `/etc/elasticsearch/jvm.options`, `/etc/logstash/jvm.options`,

To let PyAlarm send alarms to the elasticsearch, a logstash configuration file  `/etc/logstash/conf.d/maxiv.conf` is created with the following content:

```conf
input {
  tcp {
      port => 5959
      codec => json
  }
}
output {
    elasticsearch {
        hosts => ["localhost:9200"] 
        index => "tango-alarms-%{+YYYY.MM.dd}"
        document_type => "alarm"
    }
}
```

### MAX-IV PyAlarm installation

The source code in `~/work/maxiv/dev-maxiv-pyalarm` has been cloned from a MAX-IV internal repository.

- The virtual environment has been created, activated and PyAlarm installed:
  
  ```bash
  cd ~/demo/maxiv
  virtualenv --system-site-packages env
  . env/bin/activate

  cd ~/work/max-iv/dev-maxiv-pylarm
  git checkout logstash
  pip install . 
  cp -r panic/* ~/demo/maxiv/env/lib/python2.7/site-packages/panic
  ```

  the last step was necessary because, for some reason, the panic package was taken from `/usr`.

- Tango server `PyAlarm/maxiv` has been created with the *Jive* wizard. It has been
  started with `~/demo/maxiv/start.sh`, and a device `panic/demo/maxiv` added and
  configured.

- An example alarm is configured with the PANIC GUI.
  
To use the app, open *Firefox* and go to: `http://training:5601`.
  
## Notice

- All the sources are available in ~/work/.
- The MAX-IV rpm repository was not available from time to time, so some of *yum* calls where invoked with `--disaberepo=maxiv-public`.
